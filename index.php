<?php

include('models/Humain.php');
include('models/Oeuvre.php');
include('models/Auteur.php');
include('models/Exposition.php');
include('models/Commissaire.php');

include('controllers/Controller.php');
include('controllers/HomeController.php');
include('controllers/OeuvreController.php');
include('controllers/ExpositionController.php');

// Define controller and action 
if (isset($_GET['ctr']) && isset($_GET['action'])) {
    $ctr = 'App\\Controllers\\' . $_GET['ctr'];
    $controller = new $ctr();
    $action = $_GET['action'];
} else {
    $controller = new App\Controllers\HomeController();
    $action = 'home';
}

// Import Headers
require('views/header.php');

// Main content
$controller->index($action);

// Import Footer
require('views/footer.php');