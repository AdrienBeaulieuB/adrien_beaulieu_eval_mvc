<?php

namespace App\controllers;

use App\Controllers\Controller;

class HomeController extends Controller {

    public function index($action)
    {
        switch($action){
            case 'home':
                $home = 'Oui';
                require('views/home.php');
        }
    }

}