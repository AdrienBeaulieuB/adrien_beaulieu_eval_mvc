<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\Oeuvre;
use App\Models\Auteur;

class OeuvreController extends Controller{


    public function index($action)
    {
        switch($action){
            case 'list':
                $array_oeuvres = Oeuvre::getAll();
                require('views/oeuvre/listOeuvres.php');
                break;
            case 'detail':
                require('views/detailOeuvre.php');
                break;
            case 'form':
                require('views/oeuvre/formOeuvre.php');
                break;
            case 'add':
                // Register object
                if(isset($_GET['auteur'])) $auteur = $_GET['auteur'];
                if(isset($_GET['proprio'])) $proprio = $_GET['proprio'];
                if(isset($_GET['poids'])) $poids = $_GET['poids']; echo $poids;
                if(isset($_GET['desc'])) $desc = $_GET['desc']; 
                if(isset($_GET['domaine'])) $domaine = $_GET['domaine']; 

                $id = rand(0,200);
                
                $oeuvre = new Oeuvre($desc, $domaine, $id, $poids, Auteur::getAuteurById(intval($auteur)), Auteur::getAuteurById(intval($proprio)));
                $oeuvre->save();
 
                // Display list after.
                $array_oeuvres = Oeuvre::getAll();
                require('views/oeuvre/listOeuvres.php');
                break;
        }
    }
}