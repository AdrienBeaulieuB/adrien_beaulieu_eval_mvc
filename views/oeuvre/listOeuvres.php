<p><a href="index.php?ctr=OeuvreController&action=form">Ajouter</a></p>

<?php if (isset($array_oeuvres)) { ?>
    <table>
        <caption>Liste des oeuvres</caption>
            <tr>
                <th>Auteur</th>
                <th>Propriétaire</th>
                <th>Description</th>
                <th>Id</th>
                <th>Poids</th>
            </tr>
<?php
    foreach($array_oeuvres as $oeuvre){
        echo '<tr>';
        echo '<td>' . $oeuvre->getAuteur() . '</td>';
        echo '<td>' . $oeuvre->getProprio() . '</td>';
        echo '<td>' . $oeuvre->getDesc() . '</td>';
        echo '<td>' . $oeuvre->getId() . '</td>';
        echo '<td>' . $oeuvre->getPoids() . ' kg' . '</td>';
        echo '</tr>';
    }
?>
    </table>
<?php
} else {
    echo '<p>Aucune oeuvre</p>';
}
?>