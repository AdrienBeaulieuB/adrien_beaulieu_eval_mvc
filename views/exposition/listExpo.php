<?php use App\Models\Oeuvre; ?>

<p><a href="index.php?ctr=ExpositionController&action=form">Ajouter</a></p>

<?php if (isset($array_expos)) { ?>
    <table>
        <caption>Liste des expositions</caption>
            <tr>
                <th>Nom</th>
                <th>Oeuvre</th>
                <th>Nbr Salle</th>
                <th>Commissaire</th>
            </tr>
<?php
    foreach($array_expos as $expo){
        echo '<tr>';
        echo '<td>' . $expo->getName() . '</td>';
        echo '<td>'; 
        foreach($expo->getListOeuvres() as $oeuvre) { echo Oeuvre::getById($oeuvre); } 
        echo '</td>';
        echo '<td>' . count($expo->getListSalle()) . '</td>';
        echo '<td>' . $expo->getCommissaire() . '</td>';
        echo '</tr>';
    }
?>
    </table>
<?php
} else {
    echo '<p>Aucune exposition</p>';
}
?>