<?php

namespace App\Models;

use App\Models\Oeuvre;

class Salle {

    private int $id;
    private string $name;
    private array $listOeuvres;
    private bool $isOccupe;
    private bool $film = false;

    // -------
    // Methods
    // -------

    // Ajout d'oeuvre dans la salle avec quelques vérifications de faisabilité.
    public function addOeuvre(Oeuvre $oeuvre)
    {
        if ($this->isOccupe === true) return 'Impossible, la salle est déjà occupé';
        if ($oeuvre->getDomaine() == 'Film' && $this->film == false) return 'Impossible, la salle n a pas le bon matériel';

        $this->listOeuvres[] = $oeuvre;

    }

    // -----------------
    // Getters & Setters
    // -----------------

    public function getName(): string { return $this->name; }
    public function setName(string $name): self { $this->name = $name; return $this; }

    public function getIsOccupe(): bool { return $this->isOccupe; }
    public function setIsOccupe(bool $isOccupe): self { $this->isOccupe = $isOccupe; return $this; }

    public function getId(): int { return $this->id; }
    public function setId(int $id): self { $this->id = $id; return $this; }

    public function getListOeuvres(): array { return $this->listOeuvres; }
    public function setListOeuvres(array $listOeuvres): self { $this->listOeuvres = $listOeuvres; return $this; }
}