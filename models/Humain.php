<?php

namespace App\Models;

class Humain {

    private int $id;
    private string $nom;
    private string $prenom;

    /**
     * Construction de l'objet Humain
     *
     * @param string $nom
     * @param string $prenom
     */
    public function __construct(int $id, string $nom, string $prenom)
    {
        $this->setId($id);
        $this->setNom($nom);
        $this->setPrenom($prenom);
    }

    public function __toString()
    {
        return $this->getId() . ':' . $this->getNom() . ' - ' . $this->getPrenom();
    }

    // -------
    // Methods
    // -------
    
    public function save() {
        
    }

    // -----------------
    // Getters & Setters
    // -----------------

    public function getNom(): string { return $this->nom; }
    public function setNom(string $nom): self { $this->nom = $nom; return $this; }

    public function getPrenom(): string { return $this->prenom; }
    public function setPrenom(string $prenom): self { $this->prenom = $prenom; return $this; }

    public function getId(): int { return $this->id; }
    public function setId(int $id): self { $this->id = $id; return $this; }
}