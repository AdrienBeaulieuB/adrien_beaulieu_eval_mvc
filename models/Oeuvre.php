<?php

namespace App\Models;

use App\Models\Auteur;
use App\Models\Usager;
use App\Models\Humain;

class Oeuvre {

    private $auteur = null;
    private $proprio = null;
    private string $desc;
    private string $domaine;
    private int $poids;
    private int $id;

    public function __construct(
        string $desc,
        string $domaine,
        int $id,
        int $poids = 0,
        $auteur = null,
        $proprio = null,
    )
    {
        $this->setAuteur($auteur);
        $this->setProprio($proprio);
        $this->setDesc($desc);
        $this->setDomaine($domaine);
        $this->setPoids($poids);
        $this->setId($id);
    }

    public function __toString()
    {
        return '[ ' . $this->getId() . ' | ' . $this->getAuteur() . ' | ' . $this->getDomaine() . ' ]';
    }

    // -------
    // Methods
    // -------

    // retourne l'oeuvre selon son id
    static function getById(int $id)
    {
        {
            $fichier = './database/oeuvre.txt';
    
            $tab = array();
            if (file_exists($fichier)) {
                $tab = file($fichier, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES); 
            }
    
            foreach($tab as $oeuvre){
                $oeuvre = explode(',', $oeuvre);
    
                if($oeuvre[4] == $id) {
                    return new Oeuvre(
                        $oeuvre[2],
                        $oeuvre[3],
                        $oeuvre[4],
                        $oeuvre[5],
                        Auteur::getAuteurById(intval($oeuvre[0])),
                        Auteur::getAuteurById(intval($oeuvre[1]))
                    );
                }
            }
        }
    }

    // Sauvegarde dans le fichier txt
    public function save()
    {
        $string = PHP_EOL . $this->getAuteur()->getId() . ',' . $this->getProprio()->getId() . ',' . $this->getDesc() . ',' . $this->getDomaine() . ',' . $this->getId() . ',' . $this->getPoids();
        file_put_contents('./database/oeuvre.txt', $string, FILE_APPEND);
    }

    // Retourne toutes les oeuvres.
    static function getAll()
    {
        $fichier = './database/oeuvre.txt';

        $tab = array();
        if (file_exists($fichier)) {
            $tab = file($fichier, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES); 
        }

        $listOeuvre = [];

        foreach($tab as $oeuvre){
            $oeuvre = explode(',', $oeuvre);
            $listOeuvre[] = new Oeuvre(
                $oeuvre[2],
                $oeuvre[3],
                $oeuvre[4], // Id
                $oeuvre[5], // Poids
                Auteur::getAuteurById(intval($oeuvre[0])) , // Auteur
                Auteur::getAuteurById(intval($oeuvre[1])) // Propriétaire
            );
        }

        return $listOeuvre;
    }

    // -----------------
    // Getters & Setters
    // -----------------

    public function getAuteur() { return $this->auteur; }
    public function setAuteur( $auteur): self { $this->auteur = $auteur; return $this; }

    public function getProprio() { return $this->proprio; }
    public function setProprio( $proprio): self { $this->proprio = $proprio; return $this; }

    public function getDesc(): string { return $this->desc; }
    public function setDesc(string $desc): self { $this->desc = $desc; return $this; }

    public function getDomaine(): string { return $this->domaine; }
    public function setDomaine(string $domaine): self { $this->domaine = $domaine; return $this; }

    public function getId(): int { return $this->id; }
    public function setId(int $id): self { $this->id = $id; return $this; }

    public function getPoids(): int { return $this->poids; }
    public function setPoids(int $poids): self { $this->poids = $poids; return $this; }
}