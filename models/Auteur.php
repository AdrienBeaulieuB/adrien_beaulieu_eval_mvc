<?php

namespace App\Models;

use App\Models\Humain;

class Auteur extends Humain {

    private int $id;
    private int $telephone;
    private string $email;

    /**
     * Array of object Oeuvre
     *
     * @var array
     */
    private array $listOeuvresCreated;

    /**
     * Array of object Oeuvre
     *
     * @var array
     */
    private array $listOeuvresProprio;

    /**
     * Construction de l'objet Auteur
     *
     * @param integer $telephone
     * @param string $email
     * @param array $listOeuvresCreated (Default empty array)
     * @param array $listOeuvresProprio (Default empty Array)
     */
    public function __construct(
        int $id,
        string $nom,
        string $prenom,
        int $telephone, 
        string $email, 
        array $listOeuvresCreated = [], 
        array $listOeuvresProprio = []
        )
    {
        parent::__construct($id, $nom, $prenom);

        $this->setTelephone($telephone);
        $this->setEmail($email);
        $this->setListOeuvresCreated($listOeuvresCreated);
        $this->setListOeuvresProprio($listOeuvresProprio);
    }

    // -------
    // Methods
    // -------

    static function getAuteurById(int $id)
    {
        $fichier = './database/auteur.txt';

        $tab = array();
        if (file_exists($fichier)) {
            $tab = file($fichier, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES); 
        }

        foreach($tab as $auteur){
            $auteur = explode(',', $auteur);

            if($auteur[0] == $id) {
                return new Auteur(
                    $id,
                    $auteur[1],
                    $auteur[2],
                    $auteur[3],
                    $auteur[4]
                );
            }
        }
    }

    public function save()
    {

    }

    // -----------------
    // Getters & Setters
    // -----------------

    public function getTelephone(): int { return $this->telephone; }
    public function setTelephone(int $telephone): self { $this->telephone = $telephone; return $this; }

    public function getEmail(): string { return $this->email; }
    public function setEmail(string $email): self { $this->email = $email; return $this; }

    public function getListOeuvresCreated(): array { return $this->listOeuvresCreated; }
    public function setListOeuvresCreated(array $listOeuvresCreated): self { $this->listOeuvresCreated = $listOeuvresCreated; return $this; }

    public function getListOeuvresProprio(): array { return $this->listOeuvresProprio; }
    public function setListOeuvresProprio(array $listOeuvresProprio): self { $this->listOeuvresProprio = $listOeuvresProprio; return $this; }

    public function getId(): int { return $this->id; }
    public function setId(int $id): self { $this->id = $id; return $this; }
}