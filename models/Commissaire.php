<?php

namespace App\Models;

use App\Models\Humain;

class Commissaire extends Humain {

    public function __construct(int $id, string $nom, string $prenom)
    {
        parent::__construct($id, $nom, $prenom);
    }

    static function getById()
    {
        $commissaire = (new Commissaire(10, 'Elisa', 'Beaulieu'));
        return $commissaire;
    }
}