<?php

namespace App\Models;

use App\Models\Humain;

class Usager extends Humain {

    private string $email;
    
    /**
     * Array of Object Oeuvre
     *
     * @var array
     */
    private array $listOeuvres;

    /**
     * Construction objet Usager
     *
     * @param string $nom
     * @param string $prenom
     * @param string $email
     * @param array $listOeuvres ( Default empty array )
     */
    public function __construct(
        int $id,
        string $nom,
        string $prenom,
        string $email,
        array $listOeuvres = []
    )
    {
        parent::__construct($id, $nom, $prenom);

        $this->setEmail($email);
        $this->setListOeuvres($listOeuvres);
    }

    // -------
    // Methods
    // -------

    public function empreinteUneOeuvre(Oeuvre $oeuvre){

        if ($oeuvre->getPoids() > 20){
            return "Impossible, l'oeuvre est trop lourde";
        } else {
            $this->listOeuvres[] = $oeuvre;
        }
    }

    // -----------------
    // Getters & Setters
    // -----------------

    public function getListOeuvres(): array { return $this->listOeuvres; }
    public function setListOeuvres(array $listOeuvres): self { $this->listOeuvres = $listOeuvres; return $this; }

    public function getEmail(): string { return $this->email; }
    public function setEmail(string $email): self { $this->email = $email; return $this; }
}