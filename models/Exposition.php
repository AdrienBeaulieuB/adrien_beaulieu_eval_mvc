<?php

namespace App\Models;

use App\Models\Oeuvre;
use App\Models\Salle;
use App\Models\Commissaire;

class Exposition {

    private string $name;
    private array $listOeuvres;
    private array $listSalle;
    private string $theme;
    private Commissaire $commissaire;

    public function __construct(string $name, array $listOeuvres, array $listSalle, string $theme, Commissaire $commissaire)
    {
        $this->setName($name);
        $this->listOeuvres = $listOeuvres;
        $this->listSalle = $listSalle;
        $this->setTheme($theme);
        $this->setCommissaire($commissaire);
    }

    // ------
    // METHOD
    // ------

    // Enregistre une expostion
    public function save()
    {
        $string = PHP_EOL . $this->getName() . ',' . $this->getTheme() . ',' . $this->getCommissaire() . ',' . $this->getListOeuvres() . ',' . $this->getListSalle();
        file_put_contents('./database/exposition.txt', $string, FILE_APPEND);
    }

    // Ajout d'une salle à l'exposition
    public function addSalle(Salle $salle)
    {
        $this->listSalle[] = $salle->getId();

        // Update in DB
    }

    // Ajout d'une oeuvre à l'exposition
    public function addOeuvre(Oeuvre $oeuvre)
    {
        $this->listOeuvres[] = $oeuvre->getId();

        // Update in DB
    }

    // return all exposition
    static function getAll()
    {
        $fichier = './database/exposition.txt';

        $tab = array();
        if (file_exists($fichier)) {
            $tab = file($fichier, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES); 
        }

        $listexpos = [];

        foreach($tab as $expo){
            $expo = explode(',', $expo);
            
            $listexpos[] = new Exposition(
                $expo[0], // Name
                explode('-',$expo[3]), // Oeuvres
                explode('-',$expo[4]), // Salles
                $expo[1], // Theme
                Commissaire::getById(intval($expo[2])) , // Commissaire
            );
        }

        return $listexpos;
    }

    // -----------------
    // Getters & Setters
    // -----------------

    public function getListOeuvres(): array { return $this->listOeuvres; }
    public function setListOeuvres(array $listOeuvres): self { $this->listOeuvres = $listOeuvres; return $this; }

    public function getListSalle(): array { return $this->listSalle; }
    public function setListSalle(array $listSalle): self { $this->listSalle = $listSalle; return $this; }

    public function getName(): string { return $this->name; }
    public function setName(string $name): self { $this->name = $name; return $this; }

    public function getCommissaire(): Commissaire { return $this->commissaire; }
    public function setCommissaire(Commissaire $commissaire): self { $this->commissaire = $commissaire; return $this; }

    public function getTheme(): string { return $this->theme; }
    public function setTheme(string $theme): self { $this->theme = $theme; return $this; }
}